'use strict';

var gulp = require('gulp'),
	sass = require('gulp-sass'),
    rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	cssnano = require('gulp-cssnano');

gulp.task('sass', function () {
	return gulp.src('./css/*.scss')
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['last 10 versions']
		}))
		.pipe(gulp.dest('./css'))
		.pipe(cssnano({zindex: false}))
        .pipe(rename(function (path) {
            path.basename += ".min";
        }))
        .pipe(gulp.dest('./css'))
});

gulp.task('w-sass', function () {
	gulp.watch([
		'./css/*.scss',
	], {cwd: './'}, ['sass']);
});
