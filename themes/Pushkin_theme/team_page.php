<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php');?>
<?php 
function getTheURL($url_id) {
   $opg = Page::getById($url_id);
   $url=Loader::helper('navigation'); 
   $canonical=$url->getCollectionURL($opg); 
   $canonical=preg_replace("/index.php\?cID=1$/","",$canonical); 
   echo $canonical;
}
?>

		<div class="sub-banner">
			<!--IMAGE BROUGHT IN THROUGH CSS AND C5 PAGE ATTRIBUTE-->
		</div><!--end .sub-banner-->
		<div class="page-title">
			<div class="row">
				<div class="small-12 columns">
					<?php echo '<h1 class="title">' .$c->getCollectionName().'</h1>';?>
					<div class="breadcrumb">
							<?php
								$nav = BlockType::getByHandle('autonav');
								$nav->controller->orderBy = 'display_asc';
								$nav->controller->displayPages = 'top';
								$nav->controller->displaySubPages = 'relevant_breadcrumb';
								$nav->controller->displaySubPageLevels = 'all';
								$nav->render('templates/breadcrumb');
							?>
					</div>
				</div>			
			</div><!--end .row-->
		</div><!--end .page-title-->
		<div class="main-body">
			<div class="row">
				<div class="small-12 columns">
					<?php
						$areaMain  = new Area('Intro Content');
						$areaMain->display($c);
					?>
				</div>			
			</div><!--end .row-->

			<div class="row">
				<div class="team-wrapper">
				<?php
					$areaMain = new Area('Team Members');
					$areaMain->display($c);
				?>			
				</div>
			</div><!--end .row-->
		</div><!--end .main-body-->
		<script src="<?php echo $this->getThemePath();?>/js/toucheffects.js"></script>
<?php $this->inc('elements/footer.php');?>
