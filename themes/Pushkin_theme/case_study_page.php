<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php');?>

		<div class="sub-banner">
			<!--IMAGE BROUGHT IN THROUGH CSS AND C5 PAGE ATTRIBUTE-->
		</div><!--end .sub-banner-->
		<div class="page-title">
			<div class="row">
				<div class="small-12 columns">
					<?php echo '<h1 class="title">' .$c->getCollectionName().'</h1>';?>
					<div class="breadcrumb">
							<?php
								$nav = BlockType::getByHandle('autonav');
								$nav->controller->orderBy = 'display_asc';
								$nav->controller->displayPages = 'top';
								$nav->controller->displaySubPages = 'relevant_breadcrumb';
								$nav->controller->displaySubPageLevels = 'all';
								$nav->render('templates/breadcrumb');
							?>
					</div>
				</div>			
			</div><!--end .row-->
		</div><!--end .page-title-->
		<div class="main-body">
			<div class="row">
				<div class="small-12 columns">
					<?php
						$areaMain  = new Area('Intro Content');
						$areaMain->display($c);
					?>
				</div>			
			</div><!--end .row-->
			
					<div class="row">
						<div class="small-12 columns">
							<div class="cs_stripe-title">
								<h2>Public Awareness/Behavior Change</h2>
							</div><!--end .cs_stripe-title-->
						</div><!--end .sml-6-->
					</div><!--end .row-->
					<div class="row">
						<?php 	
							$a = new Area('Client 1');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-2 med-4 -->';
							}
                                                 ?>
						<?php
							$a = new Area('Client 1 1/2');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ){
								echo '<div class="small-12 medium-4 cs_img columns">';
								$a->display($c);
								echo '</div>';
							}
						?>
						<?php 	
							$a = new Area('Client 2');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-2 med-4 -->';
							}
						?>
					</div><!--end row-->
					<div class="row">
						<div class="small-12 columns">
							<div class="cs_stripe-title">
								<h2>Reputation Management</h2>
							</div>
						</div>		
					</div><!--end .row-->
					<div class="row">
						<?php 	
							$a = new Area('Client 3');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-12 med-3-->';
							}
						?>
						<?php 	
							$a = new Area('Client 3 1/2');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-12 med-4-->';
							}
						?>
						<?php 	
							$a = new Area('Client 4');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-2 med-4 -->';
							}
						?>
					</div><!--end row-->
					<div class="row">
						<div class="small-12 columns">
							<div class="cs_stripe-title">
								<h2>Brand Strategy</h2>
							</div>
						</div>	
					</div><!--end .row-->
					<div class="row">
							<?php 	
							$a = new Area('Client 5');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-2 med-4 -->';
							}
                                                ?>
						<?php
							$a = new Area('Client 5 1/2');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ){
								echo '<div class="small-12 medium-4 cs_img columns">';
								$a->display($c);
								echo '</div>';
							}
						?>
						<?php 	
							$a = new Area('Client 6');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-2 med-4 -->';
							}
						?>			
					</div><!--end .row-->
					<div class="row">
						<div class="small-12 columns">
							<div class="cs_stripe-title">
								<h2>Media Relations</h2>
							</div>
						</div>	
					</div><!--end .row-->
					<div class="row">
							<?php 	
							$a = new Area('Client 7');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-2 med-4 -->';
							}
						?>
						<?php
							$a = new Area('Client 7 1/2');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ){
								echo '<div class="small-12 medium-4 cs_img columns">';
								$a->display($c);
								echo '</div>';
							}
						?>
						<?php 	
							$a = new Area('Client 8');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="small-12 medium-4 cs_img columns">';
							    $a->display($c);
								echo '</div><!-- END sml-2 med-4 -->';
							}
						?>			
					</div><!--end .row-->
		</div><!--end .main-body-->
<?php $this->inc('elements/footer.php');?>
