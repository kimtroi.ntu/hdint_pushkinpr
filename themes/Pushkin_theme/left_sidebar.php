<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php');?>

		<div class="sub-banner">
			<!--IMAGE BROUGHT IN THROUGH CSS AND C5 PAGE ATTRIBUTE-->
		</div><!--end .sub-banner-->
		<div class="page-title">
			<div class="row">
				<div class="small-12 columns">
					<?php echo '<h1 class="title">' .$c->getCollectionName().'</h1>';?>
					<div class="breadcrumb">
							<?php
								$nav = BlockType::getByHandle('autonav');
								$nav->controller->orderBy = 'display_asc';
								$nav->controller->displayPages = 'top';
								$nav->controller->displaySubPages = 'relevant_breadcrumb';
								$nav->controller->displaySubPageLevels = 'all';
								$nav->render('templates/breadcrumb');
							?>
					</div>
				</div>			
			</div><!--end .row-->
		</div><!--end .page-title-->
		<div class="main-body">
			<div class="row">
				<div class="small-12 medium-3 columns">
					<div class="sidebar radius">
						<?php
							$areaMain = new Area('Sidebar');
							$areaMain->display($c);
						?>
					</div><!--end .sidebar-->
				</div><!--end .sml-12 med-4-->
				
				<div class="small-12 medium-9 columns">
					<?php
						$areaMain = new Area('Main Content');
						$areaMain->display($c);
					?>
				</div><!--end .sml-12 med-8-->
			</div><!--end .row-->
		</div><!--end .main-body-->
<?php $this->inc('elements/footer.php');?>