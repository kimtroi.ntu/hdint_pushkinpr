<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php');?>

		<div class="sub-banner">
			<!--IMAGE BROUGHT IN THROUGH CSS AND C5 PAGE ATTRIBUTE-->
		</div><!--end .sub-banner-->
		<div class="page-title">
			<div class="row">
				<div class="small-12 columns">
					<?php echo '<h1 class="title">' .$c->getCollectionName().'</h1>';?>
					<div class="breadcrumb">
							<?php
								$nav = BlockType::getByHandle('autonav');
								$nav->controller->orderBy = 'display_asc';
								$nav->controller->displayPages = 'top';
								$nav->controller->displaySubPages = 'relevant_breadcrumb';
								$nav->controller->displaySubPageLevels = 'all';
								$nav->render('templates/breadcrumb');
							?>
					</div>
				</div>			
			</div><!--end .row-->
		</div><!--end .page-title-->
		<div class="main-body">
			<div class="row">
				<div class="small-12 columns">
					<?php
						$areaMain  = new Area('Intro Content');
						$areaMain->display($c);
					?>
				</div>			
			</div><!--end .row-->
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 1');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 2');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 3');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 4');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
			
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 5');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 6');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 7');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 8');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 9');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 10');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 11');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 12');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 13');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 14');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 15');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 16');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 17');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item18');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 19');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 20');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 21');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 22');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 23');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 24');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 25');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 26');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 27');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 28');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 29');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 30');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 31');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 32');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
			<div class="row">
				<div class="small-12  medium-3 columns">
					<?php
					$areaMain = new Area('item 33');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 34');
					$areaMain->display($c);
					?>
				</div>		
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 35');
					$areaMain->display($c);
					?>
				</div>	
				<div class="small-12 medium-3 columns">
					<?php
					$areaMain = new Area('item 36');
					$areaMain->display($c);
					?>
				</div>		
			</div><!--end .row-->
		</div><!--end .main-body-->
<?php $this->inc('elements/footer.php');?>
