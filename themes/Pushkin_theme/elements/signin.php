<?php   defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="row">
	<div class="small-12 medium-4 columns text-left">
		<p>&copy; <?php echo date('Y')?>  <strong><a href="<?php echo DIR_REL?>/"><?php echo SITE?></a></strong>. ALL RIGHT RESERVED </p>
		<p><a href="<?php echo $this->url('/site-map')?>" >Site Map</a></p>
	</div><!--end .sml-12 med-4-->
	<div class="small-12 medium-4 columns text-center">
		<!--
<?php 
			$a = new GlobalArea('Sign In');
			$adump = $a->getAreaLayouts($c);
			if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
			    $a->setBlockLimit(1);
			    $a->display();
			} else { 
		?>
-->
		<?php   
			$u = new User();
			if ($u->isRegistered()) { 
		?>
		<?php    
			if (Config::get("ENABLE_USER_PROFILES")) {
				$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
			} else {
				$userName = $u->getUserName();
			}
		?>
		<?php   echo '<small>'.t('Currently logged in as <b>%s</b>.', $userName)?> <a href="<?php   echo $this->url('/login', 'logout')?>"><?php   echo t('Sign Out')?></a></small>
		<?php    } else { ?>
			<?php /*?><small class="show-for-large-up"><a href="<?php   echo $this->url('/login')?>"> <?php   echo t('Sign In')?></a></small><?php */?>
		<?php  } ?>
		<?php  } ?>
	</div><!--end .sml-12 med-4-->
	<div class="small-12 medium-4 columns text-right">
		<small><a href="http://www.insyntrix.com" target="_blank">Denver Website Design by Insyntrix</a></small>
	</div><!--end .sml-12 med-4-->
</div><!--end .row-->
