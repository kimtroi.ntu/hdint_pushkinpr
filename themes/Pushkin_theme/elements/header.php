<?php   defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo $this->getThemePath(); ?>/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/css/foundation.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/typography.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/css/custom.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath();?>/js/slick-1.8.1/slick/slick-theme.css">
    <script src="<?php echo $this->getThemePath(); ?>/js/vendor/modernizr.js"></script>
    <?php  Loader::element('header_required'); ?>
    <?php if($c->isEditMode()){ ?>
        <style>body{ position:static; }</style><?php
    } ?>
    <?php
    $subbanner_image = $c->getAttribute('subbanner_image');
    if ($subbanner_image) {
        $subbanner_imageURL = $subbanner_image->getURL();
        echo '<style type="text/css">.sub-banner {background-image:url(' . $subbanner_imageURL . ');background-position: center center;}</style>';
    }

    $subbanner_height = $c->getAttribute('subbanner_height');
    if ($subbanner_height) {
        echo '<style type="text/css">.sub-banner {height: '.$subbanner_height.'px;}</style>';
    }
    ?>
</head>
<body>
<header>
    <div class="row header-top hide-for-small-only">
        <div class="small-6 columns">
            <div class="logo">
                <a href="<?php echo DIR_REL?>/"><img src="<?php echo $this->getThemePath();?>/img/PushkinPR_Logo.svg" alt="Pushkin PR"/></a>
            </div><!--end .logo-->
        </div><!--end .sml-12-->
        <div class="small-6 columns">
            <div class="top-right-header">
                <div class="site-info">
                    <a href="tel:3037333441" class="phone">303-733-3441</a>
                    <a href="mailto:info@pushkinpr.com" class="email">info@pushkinpr.com</a>
                </div>
                <ul class="social-list ">
                    <li class="linkedin"><a href="https://www.linkedin.com/in/pushkinpr/" target="_blank"></a></li>
                    <li class="facebook"><a href="https://www.facebook.com/PushkinPR" target="_blank"></a></li>
                    <li class="twitter"><a href="https://twitter.com/Jon_Pushkin" target="_blank"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <?php
            $areaMain = new GlobalArea('Auto Nav');
            $areaMain->setBlockLimit(1);
            $areaMain->display($c);
            ?>
        </div>
    </div><!--end .row-->
</header>