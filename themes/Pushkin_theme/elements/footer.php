<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<footer>
    <div class="row">
        <div class="small-12 medium-4 columns">
            <h3>TWITTER</h3>
            <hr>
            <?php
            $areaMain = new GlobalArea('Twitter Feed');
            $areaMain->setBlockLimit(1);
            $areaMain->display($c);
            ?>
        </div><!--end .sml-4-->
        <div class="small-12 medium-4 columns">
            <h3>CONTACT US</h3>
            <hr>
            <?php
            $areaMain = new GlobalArea('Contact Us');
            $areaMain->setBlockLimit(1);
            $areaMain->display($c);
            ?>
        </div><!--end .sml-4-->
        <div class="small-12 medium-4 columns">
            <h3>BLOG</h3>
            <hr>
            <?php
            $areaMain = new GlobalArea('Blog');
            $areaMain->setBlockLimit(1);
            $areaMain->display($c);
            ?>
        </div><!--end .sml-4-->
    </div><!--end .row-->
</footer>
<div class="f-dark">
    <?php $this->inc('elements/signin.php');?>

</div><!--end .f-dark-->

<script src="<?php echo $this->getThemePath(); ?>/js/foundation.min.js" type="text/javascript"></script>
<?php Loader::element('footer_required') ?>
<script>
    $(document).foundation();
</script>
<!--SHARETHIS-->
<!--
    	<script type="text/javascript">stLight.options({publisher: "d84043da-a738-476a-b6f1-48a2183c2dee", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
		<script>
		var options={ "publisher": "d84043da-a738-476a-b6f1-48a2183c2dee", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "linkedin", "reddit", "blogger", "sharethis", "email"]}};
		var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
		</script>
-->

<?php if ( !isset($_GET['ctask']) ) { ?>
    <script type="text/javascript" src="<?php echo $this->getThemePath();?>/js/slick-1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo $this->getThemePath();?>/js/custom.js"></script>
<?php }; ?>

<?php if ( isset($_GET['preview']) ) { ?>
    <script src="<?php echo $this->getThemePath();?>/js/preview.js"></script>
<?php }; ?>

<!-- Start of Async HubSpot Analytics Code -->
<script type="text/javascript">
    (function(d,s,i,r) {
        if (d.getElementById(i)){return;}
        var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
        n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/1671714.js';
        e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
</script>
<!-- End of Async HubSpot Analytics Code -->
</body>
</html>