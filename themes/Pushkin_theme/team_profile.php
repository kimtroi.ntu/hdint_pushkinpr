<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php');?>

		<div class="tp-sub-banner">
			<div class="row">
				<!--IMAGE BROUGHT IN THROUGH CSS AND C5 PAGE ATTRIBUTE-->
				<div class="small-6 columns">
					<?php
						$areaMain=new Area('Profile Image');
						$areaMain->setBlockWrapperStart('<div class="tp-profile">');
						$areaMain->setBlockWrapperEnd('</div>');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);
					?>
				</div><!--end .sml-6-->
				<div class="small-6 columns">
					<div 	class="tp-details">
						<div class="team-title">
							<?php echo '<h1> <span>' .$c->getCollectionName().'</span></h1>';?>
							
						</div><!--end .team-title-->
						<div class="tp-job-title">
							<?php
								$areaMain=new Area('Job Title');
								$areaMain->setBlockLimit(1);
								$areaMain->display($c);
							?>
						</div><!--end .tp-job-title-->
					</div><!--end .TP-details-->
				</div><!--end sml-6-->
			</div><!--end .row-->
		</div><!--end .sub-banner-->
		
		<div class="page-title">
			<div class="row">
				<div class="small-12 columns">
					<h1>About</h1>
					<div class="breadcrumb">
							<?php
								$nav = BlockType::getByHandle('autonav');
								$nav->controller->orderBy = 'display_asc';
								$nav->controller->displayPages = 'top';
								$nav->controller->displaySubPages = 'relevant_breadcrumb';
								$nav->controller->displaySubPageLevels = 'all';
								$nav->render('templates/breadcrumb');
							?>
					</div>
				</div>			
			</div><!--end .row-->
		</div><!--end .page-title-->
		<div class="main-body">
			<div class="row">
				<div class="small-12 columns">
					<?php
						$areaMain=new Area('Back Link');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);
					?>
				</div><!---end .sml-12-->
			</div><!--end .row-->
			<div class="row">
				<div class="small-12 medium-9 columns">
					<div class="prof_overview">
						<?php
							$areaMain = new Area('About');
							$areaMain->display($c);
						?>
					</div><!--end .cs_overview-->
					<div class="prof_overview">
						<h2>Credentials</h2>
						<?php
						$areaMain = new Area('Credentials');
						$areaMain->display($c);
					?>
					</div><!--end .cs_overview-->
				</div><!--end .sml-12 med-8-->
				
				<div class="small-12 medium-3 columns">
					<div class="prof_sidewrap">
						<h2>Involvement</h2>
						<?php
							$areaMain=new Area('Involvement');
							$areaMain->display($c);
						?>
					</div><!--end cs_sidewrap-->
				</div><!--end .sml-12 med-4-->
			</div><!--end .row-->
		</div><!--end .main-body-->
<?php $this->inc('elements/footer.php');?>