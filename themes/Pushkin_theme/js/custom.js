jQuery(document).ready(function ($) {
    $('#ft-media .media-content').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToScroll: 1,
        slidesToShow: 4,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('#ft-media .media-content a.slick-slide').attr('target', '_blank');
});