<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

		<div class="banner-wrap">
			<div class="row">
				<div class="small-12 columns">
					<?php
						$areaMain = new Area('Banner');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);	
					?>
				</div>			
			</div><!--end .row-->
		</div><!--end .banner-wrap-->
		<div class="dark-body">
			<div class="row">
				<div class="small-12 medium-8 columns margintop">
					<div class="row">
						<div class="small-12 medium-4 columns">
							<?php
								$areaMain = new Area('Image 1');
								$areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
						<div class="small-12 medium-8 columns">
							<?php
								$areaMain = new Area('Main Content');
								$areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
					</div><!--end .row-->
					<hr>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<?php
								$areaMain = new Area('Image 2');
							    $areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
						<div class="small-12 medium-8 columns">
							<?php
								$areaMain = new Area('Sub Content');
							    $areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
					</div><!--end .row-->
				</div><!--end .sml-8-->
				
				<div class="medium-4  hide-for-small-only columns">
					<div class="featured clearfix">
						<div class="featured-title-wrap">
							<h1 class="tan">Case Studies</h1>
							<span class="line"></span>
						</div><!--end .featured-title-wrap-->
						<div class="featured-inner">
							<?php
								$areaMain = new Area('Case Studies');
							    $areaMain->display($c);
							?>
						</div><!--end .featured-inner-->
					</div><!--end .sidebar-->
				</div><!--end .sml-4-->
			</div><!--end .row-->
		</div><!--end .dark-body-->
		<div class="services-wrap">
			<div class="row">
				<div class="small-12 columns">
					<ul class="services-list">
						<li><a href="">Brand </br>Strategy</a></li>
						<li><a href="">Media </br>Relations</a></li>
						<li><a href="">Reputation Management</a></li>
						<li><a href="">Social Media Strategy</a></li>
						<li><a href="">Public </br>Awareness</a></li>
					</ul>
				</div><!--end .sml-12-->
			</div><!--end .row-->
		</div><!--end .service-wrap-->
		<div class="client-wrap">
			<div class="row">
				<div class="small-12 columns">
					<?php
						$areaMain = new Area('Client Logos');
						$areaMain->display($c);
					?>
				</div>
			</div><!--end .row-->
		</div><!--end .client-wrap-->
<?php $this->inc('elements/footer.php'); ?>