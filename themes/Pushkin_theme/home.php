<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

		<div class="banner-wrap gradient">
			
					<?php
						$areaMain = new Area('Banner');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);	
					?>
				
		</div><!--end .banner-wrap-->
		<div class="dark-body">
			<div class="row">
				<div class="small-12 large-8 columns margintop">
					<div class="row">
						<div class="small-12 medium-4 columns">
							<?php
								$areaMain = new Area('Image 1');
								$areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
						<div class="small-12 medium-8 columns">
							<?php
								$areaMain = new Area('Main Content');
								$areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
					</div><!--end .row-->
					<hr>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<?php
								$areaMain = new Area('Image 2');
							    $areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
						<div class="small-12 medium-8 columns">
							<?php
								$areaMain = new Area('Sub Content');
							    $areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
					</div><!--end .row-->
				</div><!--end .sml-8-->
				
				<div class="medium-12 large-4  hide-for-small-only columns">
					<div class="featured clearfix">
						<div class="featured-title-wrap">
							<h1 class="tan">CASE STUDIES</h1>
							<span class="line"></span>
						</div><!--end .featured-title-wrap-->
						<div class="featured-inner">
							<?php
								$areaMain = new Area('Case Studies');
							    $areaMain->display($c);
							?>
						</div><!--end .featured-inner-->
					</div><!--end .sidebar-->
				</div><!--end .sml-4-->
			</div><!--end .row-->
		</div><!--end .dark-body-->
		<div class="services-wrap">
			<div class="row">
				<ul class="services-list">
					<li><a href="/our-services/branding-strategy/">Brand </br>Strategy</a></li>
					<li><a href="/our-services/media-relations">Media </br>Relations</a></li>
					<li><a href="/our-services/reputation-management">Reputation Management</a></li>
					<li><a href="/our-services/social-media-strategy">Social Media Strategy</a></li>
					<li><a href="/our-services/public-awareness">Public </br>Awareness</a></li>
					<li><a href="/our-services/workshops">Workshops</a></li>
				</ul>
			</div><!--end .row-->
		</div><!--end .service-wrap-->
		<div class="client-wrap">
			<div class="row">
				<div class="small-6 medium-3 text-center columns">
					<?php
						$areaMain = new Area('Client Logo 1');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);
					?>
				</div><!--end .sml-12 med-3-->
				<div class="small-6 medium-3 text-center columns">
					<?php
						$areaMain = new Area('Client Logo 2');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);
					?>
				</div><!--end .sml-12 med-3-->
				<div class="small-6 medium-3 text-center columns">
					<?php
						$areaMain = new Area('Client Logo 3');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);
					?>
				</div><!--end .sml-12 med-3-->
				<div class="small-6 medium-3 text-center columns">
					<?php
						$areaMain = new Area('Client Logo 4');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);
					?>
				</div><!--end .sml-12 med-3-->
			</div><!--end .row-->
		</div><!--end .client-wrap-->
		<!--CODE FOR POP-UP WINDOW-->
			<script src="//my.hellobar.com/431342324aeaa5bc726e0cf4ec7cc663865c13d2.js" type="text/javascript" charset="utf-8" async="async"></script>
		<!--END-->
<?php $this->inc('elements/footer.php'); ?>