<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php');?>

		<div class="sub-banner">
			<!--IMAGE BROUGHT IN THROUGH CSS AND C5 PAGE ATTRIBUTE-->
		</div><!--end .sub-banner-->
		<div class="page-title">
			<div class="row">
				<div class="small-12 columns">
					<h1>Case Study</h1>
				</div>			
			</div><!--end .row-->
		</div><!--end .page-title-->
		<div class="main-body">
			<div class="row">
				<div class="small-12 columns">
					<?php
						$areaMain=new Area('Back Link');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);
					?>
				</div><!---end .sml-12-->
			</div><!--end .row-->
			<div class="row">
				<div class="small-12 medium-9 columns">
					<div class="cs_logo">
						<?php
							$areaMain = new Area('Client Logo');
							$areaMain->setBlockLimit(1);
							$areaMain->display($c);
						?>
					</div><!--end .cs_logo-->
					
					<?php echo '<h1 class="med-blue">Client: ' .$c->getCollectionName().'</h1>';?>
					<div class="breadcrumb">
							<?php
								$nav = BlockType::getByHandle('autonav');
								$nav->controller->orderBy = 'display_asc';
								$nav->controller->displayPages = 'top';
								$nav->controller->displaySubPages = 'relevant_breadcrumb';
								$nav->controller->displaySubPageLevels = 'all';
								$nav->render('templates/breadcrumb');
							?>
					</div>
					
					<div class="cs_overview">
						<?php
							$areaMain = new Area('Overview');
							$areaMain->display($c);
						?>
					</div><!--end .cs_overview-->
					<div class="cs_overview">
						<h2>The Challenge</h2>
						<?php
						$areaMain = new Area('The Challenge');
						$areaMain->display($c);
					?>
					</div><!--end .cs_overview-->
					<div class="cs_overview">
						<h2>The Solution</h2>
						<?php
							$areaMain = new Area('The Solution');
							$areaMain->display($c);	
						?>
					</div><!--end .prov_services-->
					<div class="cs_overview">
						<h2>The Results</h2>
						<?php
							$areaMain = new Area('The Results');
							$areaMain->display($c);
						?>
					</div><!--end .cs_overview-->
				</div><!--end .sml-12 med-8-->
				
				<!--
<div class="cs_divider show-for-small-only">
					&nbsp;
				</div>
-->
				<div class="small-12 medium-3 columns">
					<div class="cs_sidewrap">
						
						<?php
							$a = new Area('Gallery');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ){
								echo '<div class="cs_gallery">';
								echo '<div class="cs_side-title">';
								echo '<h3 class="white">Gallery</h3>';
								echo '</div><!--end .cs_side-title-->';
								$a->display($c);
								echo '</div><!--end .cs_gallery-->';
							}	
						?>
												
						<?php 	
							$a = new Area('Videos');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="cs_videos">';
								echo '<div class="cs_side-title">';
								echo '<h3 class="white">Videos</h3>';
								echo '</div><!--end .cs_side-title-->';
							    $a->display($c);
								echo '</div><!-- END .cs_videos-->';
							}
						?>
						<?php 	
							$a = new Area('Download PDFs');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="cs_pdfs">';
								echo '<div class="cs_side-title">';
								echo '<h3 class="white">Media Clips</h3>';
								echo '</div><!--end .cs_side-title-->';
							    $a->display($c);
								echo '</div><!-- END .cs_pdfs-->';
							}
						?>
						<?php 	
							$a = new Area('Website Link');
							$adump = $a->getAreaLayouts($c);
							if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
								echo '<div class="cs_website">';
								echo '<div class="cs_side-title">';
								echo '<h3 class="white">Website</h3>';
								echo '</div><!--end .cs_side-title-->';
							    $a->display($c);
								echo '</div><!-- END .cs_website-->';
							}
						?>
					</div><!--end cs_sidewrap-->
					
				</div><!--end .sml-12 med-4-->
			</div><!--end .row-->
		</div><!--end .main-body-->
<?php $this->inc('elements/footer.php');?>