<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>
<style>
	.flex-caption h3{display:none;}
</style>
		<div class="banner-wrap gradient">
			
					<?php
						$areaMain = new Area('Banner');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);	
					?>
				
		</div><!--end .banner-wrap-->
		<div class="">
			<div class="row">
				<div class="small-12 large-8 columns margintop">
					<div class="row">

						<div class="small-12 columns">
							<?php
								$areaMain = new Area('Main Content');
								$areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
					</div><!--end .row-->
					<hr>
					<div class="row">
						<div class="small-12 columns">
							<?php
								$areaMain = new Area('Sub Content');
							    $areaMain->display($c);
							?>
						</div><!--end .sml-12 med-8-->
					</div><!--end .row-->
				</div><!--end .sml-8-->
				
				<div class="medium-12 large-4  hide-for-small-only columns">
<!--
					<div class="awardArea" style="text-align: center; margin:20px 0; width:100%;">
						<a href="https://www.expertise.com/co/denver/public-relations-firms" style="display:inline-block; border:0;"><img style="width:200px; display:block;" width="100%" height="160" src="https://cdn.expertise.com/awards/co_denver_public-relations-firms_2017.svg" alt="Best PR Firms in Denver" /></a>
					</div>
-->
					<div class="featured clearfix">
						<div class="featured-title-wrap" style="box-shadow:none;">
							<h1 class="tan">CASE STUDIES</h1>
							<span class="line"></span>
						</div><!--end .featured-title-wrap-->
						<div class="featured-inner" style="padding-left:15px; padding-right:15px;">
							<?php
								$areaMain = new Area('Case Studies');
							    $areaMain->display($c);
							?>
						</div><!--end .featured-inner-->
					</div><!--end .sidebar-->
				</div><!--end .sml-4-->
			</div><!--end .row-->
		</div><!--end .dark-body-->
		<div class="services-wrap">
			<div class="row">
				<ul class="services-list">
					<li><a href="/our-services/brand-strategy">Brand </br>Strategy</a></li>
					<li><a href="/our-services/public-relations">Media </br>Relations</a></li>
					<li><a href="/our-services/crisis-communications">Reputation Management</a></li>
					<li><a href="/our-services/digital-strategy">Social Media Strategy</a></li>
					<li><a href="/our-services/public-awareness">Public </br>Awareness</a></li>
					<li><a href="/our-services/workshops">Workshops</a></li>
				</ul>
			</div><!--end .row-->
		</div><!--end .service-wrap-->
		
<?php $this->inc('elements/footer.php'); ?>