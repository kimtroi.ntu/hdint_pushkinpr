<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>
    <style>
        .flex-caption h3{display:none;}
    </style>

    <div class="banner-wrap gradient">
        <?php
        $areaMain = new Area('Banner');
        $areaMain->setBlockLimit(1);
        $areaMain->display($c);
        ?>
    </div><!--end .banner-wrap-->

    <?php $this->inc('sections/home-products.php') ?>

    <div class="services-wrap">
        <div class="row">
            <h2 style="margin-bottom: 20px;"><strong>Our Services</strong></h2>
            <ul class="services-list">
                <li><a href="/our-services/brand-strategy">Brand </br>Strategy</a></li>
                <li><a href="/our-services/public-relations">Public </br>Relations</a></li>
                <li><a href="/our-services/digital-strategy">Digital </br>Strategy</a></li>
                <li><a href="/our-services/crisis-communications">Crisis </br>Communications </a></li>
                <li><a href="/our-services/trainings">Trainings</a></li>
            </ul>
        </div><!--end .row-->
    </div><!--end .service-wrap-->

    <?php $this->inc('sections/home-services.php') ?>

    <?php $this->inc('sections/home-guid.php') ?>

    <?php $this->inc('sections/home-video.php') ?>

    <?php $this->inc('sections/home-media.php') ?>

<?php $this->inc('elements/footer.php'); ?>