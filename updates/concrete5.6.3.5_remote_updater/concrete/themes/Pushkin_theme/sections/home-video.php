<section id="ft-video" class="bg-blue">
    <div class="row">
        <div class="small-12 medium-6 columns" >
            <div class="video-content">
                <?php
                $areaMain  = new Area('Video content Left');
                $areaMain->display($c);
                ?>
                <div class="ft-btn">
                    <?php
                    $areaMain  = new Area('Video Button Left');
                    $areaMain->display($c);
                    ?>
                </div>
            </div>
        </div><!--end .sml-12-->

        <div class="small-12 medium-6 columns" >
            <div class="video-content">
                <?php
                $areaMain  = new Area('Video content Right');
                $areaMain->display($c);
                ?>
                <div class="ft-btn">
                    <?php
                    $areaMain  = new Area('Video Button Right');
                    $areaMain->display($c);
                    ?>
                </div>
            </div>
        </div><!--end .sml-12-->
    </div><!--end .row-->
</section>