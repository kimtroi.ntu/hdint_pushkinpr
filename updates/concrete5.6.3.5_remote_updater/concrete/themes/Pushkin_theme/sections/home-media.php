<section id="ft-media" >
    <div class="row">
        <div class="small-12 columns" >
            <h2 style="margin-bottom: 20px;"><strong>Client Media Placements</strong></h2>
        </div>
        <div class="small-12 columns" >
            <div class="row">
                <div class="media-content">
                    <?php
                    $areaMain  = new Area('Media slide 1');
                    $areaMain->display($c);
                    ?>
                </div>
            </div>
        </div><!--end .sml-12-->
    </div><!--end .row-->
</section>