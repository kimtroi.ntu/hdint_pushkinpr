<section id="ft-products">
    <div class="row">
        <div class="small-12 medium-3 columns" >
            <div class="feature-wrap" data-equalizer-watch>
                <div class="ft-img">
                    <?php
                    $areaMain  = new Area('featurePicture 1');
                    $areaMain->display($c);
                    ?>

                    <div class="ft-title">
                        <h3>Healthcare PR</h3>
                    </div><!--end .ft-title-->
                </div><!--end .ft-img-->
                <div class="ft-content">
                    <?php
                    $areaMain  = new Area('featureContent 1');
                    $areaMain->display($c);
                    ?>
                </div><!--end .ft-content-->
                <div class="ft-btn">
                    <?php
                    $areaMain  = new Area('Button 1');
                    $areaMain->display($c);
                    ?>
                </div>
            </div><!--end .feature-wrap-->
        </div><!--end .sml-12-->

        <div class="small-12 medium-3 columns" >
            <div class="feature-wrap" data-equalizer-watch>
                <div class="ft-img">
                    <?php
                    $areaMain  = new Area('featurePicture 2');
                    $areaMain->display($c);
                    ?>

                    <div class="ft-title">
                        <h3>Nonprofit PR</h3>
                    </div><!--end .ft-title-->
                </div><!--end .ft-img-->
                <div class="ft-content">
                    <?php
                    $areaMain  = new Area('featureContent 2');
                    $areaMain->display($c);
                    ?>
                </div><!--end .ft-content-->
                <div class="ft-btn">
                    <?php
                    $areaMain  = new Area('Button 2');
                    $areaMain->display($c);
                    ?>
                </div>
            </div><!--end .feature-wrap-->
        </div><!--end .sml-12-->

        <div class="small-12 medium-3 columns" >
            <div class="feature-wrap" data-equalizer-watch>
                <div class="ft-img">
                    <?php
                    $areaMain  = new Area('featurePicture 3');
                    $areaMain->display($c);
                    ?>

                    <div class="ft-title">
                        <h3>Law Firm PR</h3>
                    </div><!--end .ft-title-->
                </div><!--end .ft-img-->
                <div class="ft-content">
                    <?php
                    $areaMain  = new Area('featureContent 3');
                    $areaMain->display($c);
                    ?>
                </div><!--end .ft-content-->
                <div class="ft-btn">
                    <?php
                    $areaMain  = new Area('Button 3');
                    $areaMain->display($c);
                    ?>
                </div>
            </div><!--end .feature-wrap-->
        </div><!--end .sml-12-->

        <div class="small-12 medium-3 columns" >
            <div class="feature-wrap" data-equalizer-watch>
                <div class="ft-img">
                    <?php
                    $areaMain  = new Area('featurePicture 4');
                    $areaMain->display($c);
                    ?>

                    <div class="ft-title">
                        <h3>Government PR</h3>
                    </div><!--end .ft-title-->
                </div><!--end .ft-img-->
                <div class="ft-content">
                    <?php
                    $areaMain  = new Area('featureContent 4');
                    $areaMain->display($c);
                    ?>
                </div><!--end .ft-content-->
                <div class="ft-btn">
                    <?php
                    $areaMain  = new Area('Button 4');
                    $areaMain->display($c);
                    ?>
                </div>
            </div><!--end .feature-wrap-->
        </div><!--end .sml-12-->
    </div><!--end .row-->
</section>