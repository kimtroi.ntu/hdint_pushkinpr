<section id="ft-services" class="bg-blue">
    <div class="row">
        <div class="small-12 medium-6 columns" >
            <div class="services-content">
                <?php
                $areaMain  = new Area('Services content');
                $areaMain->display($c);
                ?>
                <div class="ft-btn">
                    <?php
                    $areaMain  = new Area('Services Button');
                    $areaMain->display($c);
                    ?>
                </div>
            </div>
        </div><!--end .sml-12-->

        <div class="small-12 medium-6 columns" >
            <div class="services-logo">
                <?php
                $areaMain  = new Area('Services logo');
                $areaMain->display($c);
                ?>
            </div>
        </div><!--end .sml-12-->
    </div><!--end .row-->
</section>