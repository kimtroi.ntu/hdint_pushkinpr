<section id="ft-guided" >
    <div class="row">
        <div class="small-12 medium-6 columns" >
            <div class="guided-logo">
                <?php
                $areaMain  = new Area('Guided logo');
                $areaMain->display($c);
                ?>
            </div>
        </div><!--end .sml-12-->

        <div class="small-12 medium-6 columns" >
            <div class="guided-content">
                <?php
                $areaMain  = new Area('Guided content');
                $areaMain->display($c);
                ?>
                <div class="ft-btn">
                    <?php
                    $areaMain  = new Area('Guided Button');
                    $areaMain->display($c);
                    ?>
                </div>
            </div>
        </div><!--end .sml-12-->
    </div><!--end .row-->
</section>